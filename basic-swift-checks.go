package main

import (
	"errors"
	"math/big"
	"regexp"
	"strconv"
)

// BICValidation is a regular expression for SWIFT/BIC validation according to ISO 9362:2022(E),
// from https://stackoverflow.com/a/73048163 (by Mue, under CC BY-SA)
var BICValidation = regexp.MustCompile("^[A-Z0-9]{4}[A-Z]{2}[A-Z0-9]{2}(?:[A-Z0-9]{3})?$")

// IBANValidation is a regular expression for simple IBAN validation - as of ISO 13616, it must start with a two-letter
// country code, then has a two-digit checksum, and then has a varying-length letter/character string.
// Additionally, the MOD97 checksum must be verified, which is not done here.
// See https://www.iban.com/structure for more information - that is also where the length 11-29 comes from.
var IBANValidation = regexp.MustCompile("^[A-Z]{2}[0-9]{2}[A-Z0-9]{11,29}$")

var Spaces = regexp.MustCompile("\\s")

// ValidateBIC validates a BIC for its general format.
func ValidateBIC(bic string) error {
	bic = Spaces.ReplaceAllString(bic, "")
	if !BICValidation.MatchString(bic) {
		return errors.New("BIC has an invalid format")
	}
	return nil
}

// ValidateIBAN validates an IBAN for its general format (no country-specific details) and its check digits.
func ValidateIBAN(iban string) error {
	iban = Spaces.ReplaceAllString(iban, "")
	if !IBANValidation.MatchString(iban) {
		return errors.New("IBAN has an invalid format")
	}
	return ValidateCheckDigits(iban)
}

// ValidateCheckDigits validates an IBAN checksum using mod97,
// from https://github.com/almerlucke/go-iban/blob/09bcab81b879a46f0bcc0b6695864b294caed7fa/iban/iban.go#L121-L157
// (by almerlucke, under The Unlicense)
func ValidateCheckDigits(iban string) error {
	// Move the four initial characters to the end of the string
	iban = iban[4:] + iban[:4]

	// Replace each letter in the string with two digits, thereby expanding the string, where A = 10, B = 11, ..., Z = 35
	mods := ""
	for _, c := range iban {
		// Get character code point value
		i := int(c)

		// Check if c is characters A-Z (codepoint 65 - 90)
		if i > 64 && i < 91 {
			// A=10, B=11 etc...
			i -= 55
			// Add int as string to mod string
			mods += strconv.Itoa(i)
		} else {
			mods += string(c)
		}
	}

	// Create bignum from mod string and perform module
	bigVal, success := new(big.Int).SetString(mods, 10)
	if !success {
		return errors.New("IBAN check digits validation failed")
	}

	modVal := new(big.Int).SetInt64(97)
	resVal := new(big.Int).Mod(bigVal, modVal)

	// Check if module is equal to 1
	if resVal.Int64() != 1 {
		return errors.New("IBAN has incorrect check digits")
	}

	return nil
}
