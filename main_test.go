package main

import (
	"net/http"
	"net/url"
	"reflect"
	"testing"
)

func Test_parseMoney(t *testing.T) {
	type args struct {
		strAmount string
	}
	tests := []struct {
		name    string
		args    args
		want    float64
		wantErr bool
	}{
		{name: "test simple value", args: args{"42"}, want: 42.0},
		{name: "test simple value with fraction", args: args{"42.23"}, want: 42.23},
		{name: "test empty amount", args: args{""}, want: 0, wantErr: true},
		{name: "test character amount", args: args{"fourtytwo"}, want: 0, wantErr: true},
		{name: "test simple value with fraction separated by comma", args: args{"42,23"}, want: 42.23},
		{name: "test simple value with fraction", args: args{"42.234"}, want: 42.0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := parseMoney(tt.args.strAmount)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseMoney() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("parseMoney() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_simpleLocaleResolver(t *testing.T) {
	type args struct {
		root   string
		locale string
		domain string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{name: "test valid input", args: args{root: "locales", locale: "de", domain: "messages"}, want: "locales/de/messages.mo"},
		{name: "test another valid input", args: args{root: "locales/foo", locale: "de", domain: "messages"}, want: "locales/foo/de/messages.mo"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := simpleLocaleResolver(tt.args.root, tt.args.locale, tt.args.domain); got != tt.want {
				t.Errorf("simpleLocaleResolver() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getMemberDataJson(t *testing.T) {
	formData := map[string]string{
		"iban":                 "DE75 5121 0800 1245 1261 99", // example from https://euipo.europa.eu/tunnel-web/secure/webdav/guest/document_library/Documents/COSME/IBAN%20numbers%20EU.pdf
		"membershipType":       "activeMember",
		"skills":               "Unit-Testing",
		"name":                 "Tester",
		"addr1":                "Auf dem Holzweg 23",
		"skillsOther":          "1",
		"frequency":            "12",
		"bic":                  "HOLZHAUSEN42",
		"timestamp":            "2022-04-11 11:07:44.196404137 +0200 CEST m=+453156.486723556",
		"registrationClientIP": "dead:beaf::1",
		"first-name":           "Johnny",
		"country":              "Deutschland",
		"memberType":           "private",
		"contribution":         "23,42",
		"email":                "unit-test@jtester.local",
		"zipcode":              "12345",
		"city":                 "Holzweghausen",
	}
	expected := `        "addr1" : "Auf dem Holzweg 23",
        "bic" : "HOLZHAUSEN42",
        "city" : "Holzweghausen",
        "contribution" : "23,42",
        "country" : "Deutschland",
        "email" : "unit-test@jtester.local",
        "first-name" : "Johnny",
        "frequency" : "12",  /* 23,42 EUR contribution every 12 month(s) */
        "iban" : "DE75******************61 99",  /* hidden here for privacy reasons */
        "memberType" : "private",
        "membershipType" : "activeMember",
        "name" : "Tester",
        "registrationClientIP" : "dead:beaf::1",
        "skills" : "Unit-Testing",
        "skillsOther" : "1",
        "timestamp" : "2022-04-11 11:07:44.196404137 +0200 CEST m=+453156.486723556",
        "zipcode" : "12345",
`

	type args struct {
		formData map[string]string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"test message", args{formData: formData}, expected},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getMemberDataJson(tt.args.formData); got != tt.want {
				t.Errorf("getMailText() = %v, want %v", got, tt.want)
			}
		})
	}
}

type mockResponseWriter struct{}

func (mockResponseWriter) Header() http.Header {
	return nil
}

func (mockResponseWriter) Write([]byte) (int, error) {
	return 1, nil
}

func (mockResponseWriter) WriteHeader(int) {}

func Test_validate(t *testing.T) {
	catalog := localeDomain.Locale([]string{"en"}...)
	request := new(http.Request)
	serverCtx := new(serverContext)
	serverCtx.writer = mockResponseWriter{}
	serverCtx.req = request
	serverCtx.locale = catalog

	// function providing copies of valid expected data
	getExpected := func() map[string]string {
		return map[string]string{
			"iban":                        "DE75 5121 0800 1245 1261 99", // example from https://euipo.europa.eu/tunnel-web/secure/webdav/guest/document_library/Documents/COSME/IBAN%20numbers%20EU.pdf
			"membership-type":             "active-member",
			"skills":                      "Unit-Testing",
			"name":                        "Tester",
			"addr1":                       "Auf dem Holzweg 23",
			"skillsOther":                 "1",
			"payment-method":              "sepa-yearly",
			"bic":                         "HOLZDE12XXX",
			"timestamp":                   "2022-04-11 11:07:44.196404137 +0200 CEST m=+453156.486723556",
			"firstName":                   "Johnny",
			"country":                     "Deutschland",
			"member-type":                 "person",
			"membership-fee-yearly-euros": "66", // rounded with %.0f
			"email":                       "unit-test@jtester.local",
			"zipcode":                     "12345",
			"city":                        "Holzweghausen",
			"registration-client-ip":      "",
			"codeberg-username":           "tester",

			"accept-bylaws":          "true",
			"accept-privacy-policy":  "true",
			"accept-sepa-conditions": "true",
		}
	}

	// function providing copies of valid input data
	getUrlValues := func() url.Values {
		return url.Values{
			"iban":                        []string{"DE75 5121 0800 1245 1261 99"},
			"membership-type":             []string{"active-member"},
			"skills":                      []string{"Unit-Testing"},
			"name":                        []string{"Tester"},
			"addr1":                       []string{"Auf dem Holzweg 23"},
			"skillsOther":                 []string{"1"},
			"payment-method":              []string{"sepa-yearly"},
			"bic":                         []string{"HOLZDE12XXX"},
			"timestamp":                   []string{"2022-04-11 11:07:44.196404137 +0200 CEST m=+453156.486723556"},
			"firstName":                   []string{"Johnny"},
			"country":                     []string{"Deutschland"},
			"member-type":                 []string{"person"},
			"membership-fee-yearly-euros": []string{"66,42"},
			"email":                       []string{"unit-test@jtester.local"},
			"zipcode":                     []string{"12345"},
			"city":                        []string{"Holzweghausen"},
			"codeberg-username":           []string{"tester"},

			"accept-bylaws":          []string{"true"},
			"accept-privacy-policy":  []string{"true"},
			"accept-sepa-conditions": []string{"true"},
		}
	}

	// TestCase 1: All values correct
	urlValues := getUrlValues()
	expected := getExpected()

	type args struct {
		serverCtx *serverContext
		postData  url.Values
	}

	type testStruct struct {
		name   string
		args   args
		want   map[string]string
		errors map[string]string
	}

	tests := []testStruct{
		{"simple test", args{serverCtx: serverCtx, postData: urlValues}, expected, map[string]string{}},
	}

	// TestCase 2: 23,42 p.a. (too small)
	expected2 := getExpected()
	expected2["membership-fee-yearly-euros"] = "23"

	urlValues2 := getUrlValues()
	urlValues2["membership-fee-yearly-euros"] = []string{"23,42"}

	expectedFeeErrorMsg := "Invalid membership fee: must be at least 24 € (100 € for organizations) unless a discounted membership fee is requiested."
	expectedSEPAErrorMsg := "When using SEPA payments, each payment must be at least 10 €. Please choose e.g. yearly payments to account for this."
	tests = append(tests, testStruct{"too small yearly fee", args{serverCtx: serverCtx, postData: urlValues2}, expected2, map[string]string{"membership-fee-yearly-euros": expectedFeeErrorMsg}})

	// TestCase 3: 9,99 p.m. (too small)
	urlValues3 := getUrlValues()
	urlValues3["membership-fee-yearly-euros"] = []string{"9,99"}
	urlValues3["payment-method"] = []string{"sepa-monthly"}

	expected3 := getExpected()
	expected3["payment-method"] = "sepa-monthly"
	expected3["membership-fee-yearly-euros"] = "10"
	tests = append(tests, testStruct{"too small monthly fee", args{serverCtx: serverCtx, postData: urlValues3}, expected3, map[string]string{
		"membership-fee-yearly-euros": expectedFeeErrorMsg,
		"payment-method":              expectedSEPAErrorMsg,
	}})

	// TestCase 4: no data
	tests = append(tests, testStruct{"no data", args{serverCtx: serverCtx, postData: url.Values{}}, map[string]string{"membership-fee-yearly-euros": "0", "registration-client-ip": "", "timestamp": "automagic"}, map[string]string{
		"membership-fee-yearly-euros": expectedFeeErrorMsg,

		"accept-bylaws":         "You must agree to our bylaws.",
		"accept-privacy-policy": "You must agree to our privacy policy.",
		"company-name":          "A company name is required for legal entities.",
	}})

	// TestCase 5: empty value
	urlValues4 := getUrlValues()
	urlValues4["membership-fee-yearly-euros"] = []string{}

	expected4 := getExpected()
	expected4["membership-fee-yearly-euros"] = "0"
	tests = append(tests, testStruct{"empty contribution", args{serverCtx: serverCtx, postData: urlValues4}, expected4, map[string]string{
		"membership-fee-yearly-euros": expectedFeeErrorMsg,
		"payment-method":              expectedSEPAErrorMsg,
	}})

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, errors := validate(tt.args.serverCtx, tt.args.postData)
			// the timestamp is always expected to be correct
			tt.want["timestamp"] = got["timestamp"]
			if !reflect.DeepEqual(errors, tt.errors) {
				t.Errorf("errors got = %v, want %v", errors, tt.errors)
				mapDiff(t, errors, tt.errors)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("validate() got = %v, want %v", got, tt.want)
				mapDiff(t, got, tt.want)
			}
		})
	}
}

func mapDiff(t *testing.T, got, want map[string]string) {
	t.Helper()
	for k, w := range want {
		g, ok := got[k]
		if !ok {
			t.Logf("[%s] missing (want %v)", k, w)
			continue
		}
		if g != w {
			t.Logf("[%s] got = %v, want %v", k, g, w)
		}
	}
	for k, g := range got {
		_, ok := want[k]
		if !ok {
			t.Logf("[%s] unexpected %v", k, g)
		}
	}
}

func Test_parseEmail(t *testing.T) {
	tests := []struct {
		input string
		ok    bool
	}{
		{input: "example@gmail.com", ok: true},
		{input: "example+firstname+lastname@email.com", ok: true},
		{input: "example@234.234.234.234", ok: true},
		{input: "A special string!", ok: false},
	}
	for _, tt := range tests {
		locale := localeDomain.Locale("en")
		t.Run(tt.input, func(t *testing.T) {
			err := checkEmail(locale, tt.input)
			if tt.ok && err != nil {
				t.Errorf("input is bad, expected to be good.")
			} else if !tt.ok && err == nil {
				t.Errorf("input is good, expected to be bad.")
			}
		})
	}
}
