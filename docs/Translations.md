# Translations

This tool uses gettext-compatible translation files. See the [go-gettext-module documentation](https://github.com/snapcore/go-gettext) for further details.

Basically, it expects language files in locales/{de,en,...}/messages.mo.

The source translation files are located in the `./locales` folder with a `.po` extension.

## Compiling `.mo` files

The .mo files can be generated from the .po files using msgfmt, which is also called like this by simply running `go generate`:

```shell
find locales -name "*.po" -execdir msgfmt "{}" ";"
```

## Contributing translations

Codeberg manages translations for Registration Server using [Codeberg Translate](https://translate.codeberg.org/projects/codeberg/ev-registration-server/), which is a [Weblate](https://weblate.org/) server.

To contribute language translations, submit them on Codeberg Translate.
