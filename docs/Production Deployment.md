# Production Deployment

You may want to start `ssh-agent` and do `ssh-add` before calling `make deployment`, to avoid typing your ssh passphrase several times. Then do:

```shell
make -C HOSTNAME_FQDN=<hostname> deployment
```
