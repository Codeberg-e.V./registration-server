{{ template "header" }}

		<form id="registration-form" class="container-fluid" method="post" action="/post">
			<div class="content">
				<h1 class="content-title">{{ Gettext "Become a Member" }}</h1>
				<p>{{ Gettext "Become a Member (Markdown Description)" | Markdown }}</p>

				{{ if len .errors }}
					<div class="alert alert-danger mb-20 pb-0" role="alert">
						<h4 class="alert-heading">{{ Gettext "Errors occurred during registration" }}</h4>
						<ul>
							{{ range $key, $message := .errors }}
								<li>{{ PGettext "Validation Error" $message }}</li>
							{{ end }}
						</ul>
						<p>{{ Gettext "If you press the back button of your browser you can correct your input." }}</p>
					</div>
				{{ end }}

				<div class="row">
					<div class="col-xl">
						<div class="card m-0 mb-20" id="ch-membership">
							<h2 class="card-title">
								{{ Gettext "Membership Type" }}
							</h2>
							{{ Gettext "Membership Type (Markdown Description)" | Markdown }}
							<hr class="my-15">
							<div class="form-group">
								<label for="member-type" class="required">{{ Gettext "Who or what are you?" }}</label>
								<select class="form-control" name="member-type" id="member-type" required="required"
										onchange="
											const slider = document.getElementById('membership-fee-slider');
											const fee = document.getElementById('membership-fee-yearly-euros');
											const discount = document.getElementById('apply-for-discounted-membership');
											const type = document.getElementById('membership-type');
											slider.min = value === 'company' ? 100 : 24;
											slider.max = value === 'company' ? 2400 : 240;
											fee.value = slider.value = value === 'company' ? 100 : 48;
											if (value === 'company' && discount.checked) discount.checked = false;
											type.value = value === 'company' ? 'supporting-member' : 'active-member';
										">
									<option value="person" selected="selected">{{ Gettext "Private Individual" }}</option>
									<option value="company">{{ Gettext "Legal Entity (e.g. organization or company)" }}</option>
								</select>
								<script>window.addEventListener("load", () => document.getElementById('member-type').onchange())</script>
							</div>
							<div hidden-if="val('member-type') == 'company'">
								<div class="form-group mb-0">
									<label for="membership-type" class="required">{{ Gettext "What kind of member do you want to become?" }}</label>
									<select class="form-control" name="membership-type" id="membership-type" required="required">
										<option value="active-member" disabled-if="val('member-type') === 'company'" selected="selected">{{ Gettext "Active Member" }}</option>
										<option value="supporting-member">{{ Gettext "Supporting Member" }}</option>
									</select>
								</div>
							</div>
						</div>
						<div class="card m-0 mb-20" id="ch-volunteer-work">
							<h2 class="card-title">
								{{ Gettext "Volunteer Work" }}
							</h2>
							{{ Gettext "Volunteer Work (Markdown Description)" | Markdown }}
							<hr class="my-15">
							<div class="form-group mb-0">
								<label>{{ Gettext "I'd like to support you in the following areas:" }}</label>
								{{ range Options "application-development" "it-security" "database-engineering" "distributed-filesystems" "cluster-infrastructure" "bookkeeping-and-finances" "legal-stuff" "public-relations" "fundraising" }}
								<div class="custom-checkbox mb-5">
									<input type="checkbox" value="true" name="volunteer-work[{{ . }}]" id="volunteer-work[{{ . }}]">
									<label for="volunteer-work[{{ . }}]">{{ PGettext "Volunteer Work Options" . }}</label>
								</div>
								{{ end }}
								<div class="form-group mt-15 mb-0">
									<label for="volunteer-work[other]">{{ PGettext "Volunteer Work Options" "Other areas where I can help:" }}</label>
									<textarea class="form-control" name="volunteer-work[other]" id="volunteer-work[other]"></textarea>
								</div>
							</div>
						</div>
						<div class="card m-0 mb-20" id="ch-personal">
							<h2 class="card-title">
								{{ Gettext "Personal Information" }}
							</h2>
							<div class="form-row row-eq-spacing-sm">
								<div class="col-sm">
									<label for="first-name" class="required" required-if="val('member-type') === 'person'">{{ Gettext "First Name:" }}</label>
									<input type="text" class="form-control" name="first-name" id="first-name" required="required" required-if="val('member-type') === 'person'">
								</div>
								<div class="col-sm">
									<label for="last-name" class="required" required-if="val('member-type') === 'person'">{{ Gettext "Last Name:" }}</label>
									<input type="text" class="form-control" name="last-name" id="last-name" required="required" required-if="val('member-type') === 'person'">
								</div>
								<div class="col-sm">
									<label for="preferred-name">{{ Gettext "Preferred Name:" }}</label>
									<input type="text" class="form-control" name="preferred-name" id="preferred-name">
								</div>
							</div>
							<div class="form-group" hidden-if="val('member-type') === 'person'">
								<label for="company" required-if="val('member-type') === 'company'">{{ Gettext "Organization or Company:" }}</label>
								<input type="text" class="form-control" name="company-name" id="company-name" required-if="val('member-type') === 'company'">
							</div>
							{{ Gettext "Email Address (Markdown Description)" | Markdown }}
							<div class="form-group">
								<label for="email" class="required">{{ Gettext "Email Address:" }}</label>
								<input type="email" class="form-control" name="email-address" id="email-address" required="required">
							</div>
							<hr class="my-15">
							{{ Gettext "Postal Address (Markdown Description)" | Markdown }}
							<div class="form-group">
								<label for="street" class="required">{{ Gettext "Street Address:" }}</label>
								<input type="text" class="form-control" name="street-address" id="street-address" required="required">
							</div>
							<div class="form-group">
								<label for="street-detail">{{ Gettext "Address Details:" }}</label>
								<input type="text" class="form-control" name="address-detail" id="address-detail">
							</div>
							<div class="form-row row-eq-spacing-sm">
								<div class="col-sm-3">
									<label for="postal-code">{{ Gettext "Postal Code:" }}</label>
									<input type="text" class="form-control" name="postal-code" id="postal-code">
								</div>
								<div class="col-sm-9">
									<label for="city" class="required">{{ Gettext "City:" }}</label>
									<input type="text" class="form-control" name="city" id="city" required="required">
								</div>
							</div>
							<div class="form-group mb-0">
								<label for="country" class="required">{{ Gettext "Country:" }}</label>
								<input type="text" class="form-control" name="country" id="country">
							</div>
                            <hr class="my-15">
                            {{ Gettext "Codeberg Username (Markdown Description)" | Markdown }}
                            <div class="form-group mb-0">
								<label for="codeberg-username">{{ Gettext "Codeberg Username:" }}</label>
								<input type="text" class="form-control" name="codeberg-username" id="codeberg-username">
							</div>
							<hr class="my-15">
							{{ Gettext "Accept Privacy Policy (Markdown Description)" | Markdown }}
							<div class="custom-checkbox">
								<input type="checkbox" name="accept-privacy-policy" id="accept-privacy-policy" value="true" required="required">
								<label for="accept-privacy-policy" class="required">{{ Gettext "I accept the privacy policy" }}</label>
							</div>
						</div>

						<div class="card m-0 mb-20" id="ch-fee">
							<h2 class="card-title">
								{{ Gettext "Membership Fee" }}
							</h2>
							{{ Gettext "Membership Fee Information (Markdown Description)" | Markdown }}
							<div hidden-if="val('member-type') == 'person'">
								{{ Gettext "Additional Membership Fee Information for Legal Entities (Markdown Description)" | Markdown }}
							</div>

							<div hidden-if="val('member-type') != 'person'">
								<div class="form-group custom-checkbox">
									<input type="checkbox" name="apply-for-discounted-membership" id="apply-for-discounted-membership" value="true" onchange="document.getElementById('membership-fee-yearly-euros').value = document.getElementById('membership-fee-slider').value = this.checked ? 12 : 48">
									<label for="apply-for-discounted-membership">{{ Gettext "I want to apply for a discounted membership fee" }}</label>
								</div>
							</div>

							<div hidden-if="!val('apply-for-discounted-membership')">
								{{ Gettext "Discounted Membership Information (Markdown Description)" | Markdown }}
								<div class="form-group">
									<label for="reason-for-discounted-membership" required-if="val('apply-for-discounted-membership')">{{ Gettext "Why do you want to pay a discounted membership fee?" }}</label>
									<textarea class="form-control" name="reason-for-discounted-membership" id="reason-for-discounted-membership" required-if="val('apply-for-discounted-membership')"></textarea>
								</div>
							</div>

							<div hidden-if="val('apply-for-discounted-membership')">
								<hr class="my-15">

								<div class="form-group mb-5">
									<label for="membership-fee-yearly-euros" class="required">{{ Gettext "Chosen yearly membership fee:" }}</label>
									<div class="form-row mb-10">
										<div class="col mr-10" style="padding-top: 1px" hidden hidden-if="false"><!-- only show with JavaScript enabled -->
											<input id="membership-fee-slider" type="range" value="48" step="2" min="24" max="240" oninput="document.getElementById('membership-fee-yearly-euros').value = this.value">
										</div>
										<div class="col pr-0" style="max-width: 6.5em;">
											<div class="input-group">
												<input class="form-control" type="text" inputmode="numeric" name="membership-fee-yearly-euros" id="membership-fee-yearly-euros" value="48" required-if="!val('apply-for-discounted-membership')" onchange="document.getElementById('membership-fee-slider').value = this.value">
												<div class="input-group-append">
													<span class="input-group-text">€</span>
												</div>
											</div>
										</div>
									</div>
									<div class="font-size-12 text-muted" hidden-if="true"><!-- only show without JavaScript -->
										{{ Gettext "When applying for a discounted membership, the fee will always be exactly 12 € per year!" }}
									</div>
								</div>
								<!--<div class="card row mx-0 p-0">
									<div class="col-3 p-15 pr-0">
										<img src="https://cdn1.iconfinder.com/data/icons/filled-line-christmas-icons/75/_deer_smile-512.png" style="width: 100%;">
									</div>
									<div class="col-9 p-15">
										<h2 class="card-title mb-0">
											Du bist toll!
										</h2>
										<p class="mt-5 mb-0">
											Mit deinem Beitrag können sich unsere virtuellen Rentiere jede Woche einen neuen
											Witz für diese fühl-dich-gut-während-du-Geld-ausgibst-Box ausdenken!
										</p>
									</div>
								</div>-->
							</div>
							<div>
								<hr class="mb-15 mt-10">

								<div class="form-group">
									<label for="payment-method" class="required">{{ Gettext "Payment Method (SEPA transactions must be at least 10 €):" }}</label>
									<select class="form-control" name="payment-method" id="payment-method" required-if="!val('apply-for-discounted-membership')">
										<option value="sepa-yearly" selected="selected" content-from="this.getAttribute('data-translation').replace('%s', fmt(val('membership-fee-yearly-euros') / 1, 0, 2))" data-translation="{{ PGettext "Payment Method" "SEPA Direct Debit (%s € yearly)" }}">{{ PGettext "Payment Method" "SEPA Direct Debit (yearly)" }}</option>
										<option value="sepa-half-yearly" disabled-if="val('apply-for-discounted-membership') || val('membership-fee-yearly-euros') < 20" content-from="this.getAttribute('data-translation').replace('%s', fmt(val('membership-fee-yearly-euros') / 2, 0, 2))" data-translation="{{ PGettext "Payment Method" "SEPA Direct Debit (%s € half-yearly)" }}">{{ PGettext "Payment Method" "SEPA Direct Debit (half-yearly)" }}</option>
										<option value="sepa-quarterly" disabled-if="val('apply-for-discounted-membership') || val('membership-fee-yearly-euros') < 40" content-from="this.getAttribute('data-translation').replace('%s', fmt(val('membership-fee-yearly-euros') / 4, 0, 2))" data-translation="{{ PGettext "Payment Method" "SEPA Direct Debit (%s € quarterly)" }}">{{ PGettext "Payment Method" "SEPA Direct Debit (quarterly)" }}</option>
										<option value="sepa-monthly" disabled-if="val('apply-for-discounted-membership') || val('membership-fee-yearly-euros') < 120" content-from="this.getAttribute('data-translation').replace('%s', fmt(val('membership-fee-yearly-euros') / 12, 0, 2))" data-translation="{{ PGettext "Payment Method" "SEPA Direct Debit (%s € monthly)" }}">{{ PGettext "Payment Method" "SEPA Direct Debit (monthly)" }}</option>
										<option value="wire-transfer">{{ PGettext "Payment Method" "Manual Bank Transfer" }}</option>
									</select>
								</div>
							</div>
							<div>
								<div hidden-if="val('payment-method') !== 'wire-transfer'">
									{{ Gettext "Manual Bank Transfer Information (Markdown Description)" | Markdown }}

									<pre class="code mb-15">IBAN DE90 8306 5408 0004 1042 42<br>BIC  GENODEF1SLR</pre>
								</div>

								<div hidden-if="!val('payment-method').startsWith('sepa-') && val('payment-method') !== ''">
									<div class="form-group">
										<label for="iban" required-if="val('payment-method').startsWith('sepa-')">IBAN:</label>
										<!-- TODO: use a field that adds spaces automatically -->
										<input type="text" class="form-control" name="iban" id="iban" required-if="val('payment-method').startsWith('sepa-')">
									</div>
									<div class="form-group">
										<label for="bic" required-if="val('payment-method').startsWith('sepa-')">BIC:</label>
										<input type="text" class="form-control" name="bic" id="bic" required-if="val('payment-method').startsWith('sepa-')">
									</div>
									<div class="form-group custom-checkbox">
										<input type="checkbox" name="accept-sepa-conditions" id="accept-sepa-conditions" value="true" required-if="val('payment-method').startsWith('sepa-')">
										<label for="accept-sepa-conditions" required-if="val('payment-method').startsWith('sepa-')">{{ Gettext "SEPA Disclaimer (Markdown)" | Markdown }}</label>
									</div>
								</div>
							</div>
						</div>
						<div class="card m-0 mb-20">
							<h2 class="card-title">
								{{ Gettext "Submit Membership Application" }}
							</h2>
							<div class="custom-checkbox form-group">
								<input type="checkbox" name="accept-bylaws" id="accept-bylaws" value="true" required="required">
								<label for="accept-bylaws" class="required">
									{{ Gettext "Accept Bylaws (Markdown Description)" | Markdown }}
								</label>
							</div>
							<button class="btn btn-primary btn-lg w-full" type="submit">{{ Gettext "Apply for Membership!" }}</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script>
			[...document.getElementById("registration-form").querySelectorAll("a[href]")].forEach(a => { a.target = "_BLANK" });
		</script>
{{ template "footer" }}
