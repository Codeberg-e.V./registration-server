.PHONY: all clean deployment deploy-reg-server locales check

SSH_OPTIONS := -J jump@codeberg.in-berlin.de:19198
SSH_TARGET := root@ev.lxc.local

BUILDDIR := ./build

GOROOT ?= $(shell go env GOROOT || echo /usr/lib/go)
GO ?= ${GOROOT}/bin/go
GOPATH ?= ${BUILDDIR}/gopath

TARGETS = ${BUILDDIR}/reg-server

all : ${TARGETS}

${BUILDDIR}/reg-server : *.go go.mod go.sum locales templates
	mkdir -p ${BUILDDIR}
	${GO} mod tidy
	${GO} generate .
	CGO_ENABLED=0 ${GO} build -ldflags "-s -w" -o $@ .

clean :
	@rm -f ${BUILDDIR}
	@find locales -name "*.mo" -exec rm "{}" ";"

check :
	${GO} test -v

lint :
	gofumpt -extra -w .

deployment : deploy-reg-server

deploy-reg-server : ${BUILDDIR}/reg-server
	-ssh ${SSH_OPTIONS} ${SSH_TARGET} systemctl stop reg-server
	rsync -av -e "ssh ${SSH_OPTIONS}" --chown=root:root $< ${SSH_TARGET}:/usr/local/bin/
	ssh ${SSH_OPTIONS} ${SSH_TARGET} systemctl daemon-reload
	ssh ${SSH_OPTIONS} ${SSH_TARGET} systemctl enable --now reg-server
	ssh ${SSH_OPTIONS} ${SSH_TARGET} systemctl status reg-server
