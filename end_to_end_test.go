// SPDX-FileCopyrightText: 2023 Olivier Charvin <git@olivier.pfad.fr>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/http/httptest"
	"net/mail"
	"net/url"
	"os/exec"
	"reflect"
	"strings"
	"testing"
	"time"
)

func TestEndToEndSuccess(t *testing.T) {
	environmentDefaults["GPG_KEYRING_PATH"] = "./testdata/keyring/pubring.kbx"
	environmentDefaults["MAIL_REGISTRATION_RECIPIENT"] = "codeberg@example.org"
	makeGET := func(target string) *httptest.ResponseRecorder {
		req := httptest.NewRequest("GET", target, nil)
		resp := httptest.NewRecorder()
		dispatcher(resp, req)
		return resp
	}

	type email struct {
		from    string
		to      []string
		subject string
		body    []byte
	}
	emails := make(chan email, 2) // 1 to the admins, 1 to the user
	defaultSendMail = func(from string, to []string, body []byte) error {
		t.Helper()
		msg, err := mail.ReadMessage(bytes.NewReader(body))
		if err != nil {
			t.Fatal(err)
		}
		date, err := msg.Header.Date()
		if err != nil {
			t.Fatal(err)
		}
		if date.IsZero() {
			t.Fatal("unexpected zero date")
		}
		parsedFrom, err := msg.Header.AddressList("From")
		if err != nil {
			t.Fatal(err)
		}
		if len(parsedFrom) != 1 {
			t.Fatalf("unexpected From length: %d", len(parsedFrom))
		}
		if from != parsedFrom[0].Address {
			t.Fatalf("From: expected %q, got %q", from, parsedFrom[0].Address)
		}

		parsedTo, err := msg.Header.AddressList("To")
		if err != nil {
			t.Fatal(err)
		}
		if len(parsedTo) != len(to) {
			t.Fatalf("unexpected To length: %d, expected %d", len(parsedTo), len(to))
		}
		for i := range to {
			if to[i] != parsedTo[i].Address {
				t.Fatalf("To: expected %q, got %q", to[i], parsedTo[i].Address)
			}
		}

		msgBody, err := io.ReadAll(msg.Body)
		if err != nil {
			t.Fatal(err)
		}

		emails <- email{
			from:    from,
			to:      to,
			subject: msg.Header.Get("Subject"),
			body:    msgBody,
		}
		return nil
	}
	defer func() {
		select {
		case e := <-emails:
			t.Fatalf("unexpected pending email (after test): %v", e)
		default:
		}
	}()
	makePOST := func(target string, data url.Values) *httptest.ResponseRecorder {
		req := httptest.NewRequest("POST", target, strings.NewReader(data.Encode()))
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
		resp := httptest.NewRecorder()
		dispatcher(resp, req)
		return resp
	}

	// load form
	resp := makeGET("/")
	if resp.Result().StatusCode != http.StatusOK {
		t.Fatalf("unexpected status %d", resp.Result().StatusCode)
	}

	// submit form
	resp = makePOST("/post", url.Values{
		"member-type": []string{"person"},
		// "first-name":  []string{"Max"},
		// "last-name":     []string{"Mustermann"},
		"email-address": []string{"max@example.org"},

		"membership-fee-yearly-euros": []string{"48"},

		"accept-privacy-policy": []string{"true"},
		"accept-bylaws":         []string{"true"},
	})
	if resp.Result().StatusCode != http.StatusFound {
		t.Fatalf("unexpected status %d", resp.Result().StatusCode)
	}
	thanksURL := "/thanks"
	if loc := resp.Result().Header.Get("Location"); loc != thanksURL {
		t.Fatalf("unexpected Location %s", loc)
	}

	// load thanks
	resp = makeGET(thanksURL)
	if resp.Result().StatusCode != http.StatusOK {
		t.Fatalf("unexpected status %d", resp.Result().StatusCode)
	}
	if strings.Contains(resp.Body.String(), "<form") { // trick to ensure that the landing page was not rendered
		t.Fatalf("unexpected <form on thanks page: %s", resp.Body.String())
	}

	nextEmail := func() email {
		t.Helper()
		select {
		case <-time.After(time.Second):
			t.Fatal("no email received after 1s")
		case e := <-emails:
			return e
		}
		return email{}
	}

	// check admin email
	adminEmail := nextEmail()
	if !reflect.DeepEqual(adminEmail.to, []string{"codeberg@example.org"}) {
		t.Fatalf("admin-email: unexpected to: %v", adminEmail.to)
	}
	if adminEmail.from != "codeberg@codeberg.org" {
		t.Fatalf("admin-email: unexpected from: %v", adminEmail.from)
	}
	if adminEmail.subject != "User registration" {
		t.Fatalf("admin-email: unexpected subject: %q", adminEmail.subject)
	}

	clearBody := decryptEmail(t, adminEmail.body)
	var decodedBody map[string]string
	// ignore JSON decoding error (for nested "new-data-format")
	json.NewDecoder(bytes.NewReader(clearBody)).Decode(&decodedBody)
	checkValue := func(key, expectedValue string) {
		t.Helper()
		if decodedBody[key] != expectedValue {
			t.Errorf("[%s] expected %q, got %q", key, expectedValue, decodedBody[key])
		}
	}
	checkValue("contributionCustom", "48.00")
	checkValue("frequency", "12")
	checkValue("memberType", "private")
	checkValue("email", "max@example.org")

	// check user email
	userEmail := nextEmail()
	if !reflect.DeepEqual(userEmail.to, []string{"max@example.org"}) {
		t.Fatalf("user-email: unexpected to: %v", userEmail.to)
	}
	if userEmail.from != "codeberg@codeberg.org" {
		t.Fatalf("user-email: unexpected from: %v", userEmail.from)
	}
	if userEmail.subject != "Welcome to Codeberg" {
		t.Fatalf("user-email: unexpected subject: %q", userEmail.subject)
	}
	// TODO: perfom some check on the user email
}

func decryptEmail(t *testing.T, body []byte) []byte {
	cmd := exec.Command("gpg", "--homedir", "./testdata/keyring", "--decrypt")
	cmd.Stdin = bytes.NewReader(body)
	out, err := cmd.Output()
	if err != nil {
		exitErr := &exec.ExitError{}
		if errors.As(err, &exitErr) {
			t.Log(string(exitErr.Stderr))
		}
		t.Fatal(err)
	}
	return out
}
